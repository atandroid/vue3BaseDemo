import { fetchStreamData } from "./request";

//测试接口
export const getTest = (token, onData, onComplete) => {
    // 使用token构建params对象
    const params = { token: token };

    // 调用fetchStreamData，并传入构建好的params
    fetchStreamData("/stream", params, onData, onComplete);
}

//同步feed列表
export const syncFeed = (onData, onComplete) => {
    // 调用fetchStreamData，并传入构建好的params
    fetchStreamData("/start_syncfeed", null, onData, onComplete);
}