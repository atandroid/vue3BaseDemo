import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../components/HomeView.vue'
import AboutView from '../components/AboutView.vue'
import ReSizeView from '../components/ReSizeView.vue'
import TestPinia from '../components/TestPinia.vue'

const routes = [
    {
        path: '/',
        name: 'Home',
        component: HomeView
    },
    {
        path: '/about',
        name: 'About',
        component: AboutView
    },
    {
        path: '/testPinia',
        name: 'testPinia',
        component: TestPinia
    },
    {
        path: '/more',
        name: 'More',
        component: ReSizeView
    }
]

const router = createRouter({
    history: createWebHistory(import.meta.env.BASE_URL),
    routes
})

export default router
