import { defineStore } from 'pinia'
export const usePiniaLocalTestStore = defineStore('piniaLocalTest', {
    state: () => {
        return{
            testText: "",
            localText: "",
        }
    },
    persist: true,
    getters: {

    },
    actions: {

    },
})