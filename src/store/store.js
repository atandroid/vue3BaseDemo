import { defineStore } from 'pinia'
export const usePiniaTestStore = defineStore('piniaTest', {
    state: () => {
        return{
            testText: "",
            localText: "",
        }
    },
    persist: {
        paths: ['localText'],
    },
    getters: {

    },
    actions: {

    },
})